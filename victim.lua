local class = require 'lib/middleclass'

local Victim = class('Victim')

Victim.static.sounds = {
	slurp = love.audio.newSource('assets/slurp.wav')
}

function Victim:initialize(x, y)
	self.dead = false
	self.pos = {
		x = x,
		y = y
	}
	self.health = love.math.random(90, 120)

	self.images = {
		live = love.graphics.newImage('assets/spectator.png'),
		dead = love.graphics.newImage('assets/spectator_dead.png')
	}

	Victim.sounds.slurp:setVolume(0.05)
	self.lastSlurped = love.timer.getTime()
end

function Victim:randomlyBleed(chance, distance)
	local dice = love.math.random()

	if self.dead == true then
		return
	end

	self.health = self.health - dice * chance / distance * 200 * 10

	if self.health < 0 then
		self.dead = true
		OD.game:registerDeath()
	end
end

function Victim:update(dt)

	if self.dead == true then
		return
	end

	local dice = love.math.random()

	if self.lastSlurped + 2.0 < love.timer.getTime() then
		if dice < 0.1 then
			Victim.sounds.slurp:play()
			self.lastSlurped = love.timer.getTime()
		end
	end
end

function Victim:draw()
	-- local color
	-- if self.dead == true then
	-- 	color = {240, 20, 20}
	-- elseif self.dead == false then
	-- 	color = {20, 240, 20}
	-- end

	-- love.graphics.rectangle('fill', self.pos.x, self.pos.y, 20, 20)
	-- love.graphics.circle('fill', self.pos.x, self.pos.y, 16, 16)

	local img
	if self.dead == false then
		img = self.images.live
		love.graphics.setColor(255,255,255)
	else
		img = self.images.dead
		love.graphics.setColor(255,255,255, 150)
	end
	love.graphics.draw(img, self.pos.x, self.pos.y)	

	if OD._debug == true then
		love.graphics.print(math.floor(self.health), self.pos.x - 10, self.pos.y - 20)
	end
end

return Victim