local class = require 'lib/middleclass'
local singer = require 'singer'
local victim = require 'victim'
local notes = require 'notes'

local Game = class('Game')

function Game:initialize()
	self.singer = singer:new(0)
	self.victims = {}

	self.timeLeft = 60

	local margin = 0.1 * OD.width
	local b = (-8*64 - 2*margin + OD.width)/7
	for i=1,8 do
		for j=1,6 do
			self.victims[i+8*(j-1)] = victim:new(margin + (64+b)*(i-1), 300 + 40*(j-1))
		end
	end

	self.notes = notes:new()
	self.timerStart = love.timer.getTime()
	self.currentTime = 0
	self.over = false
	self.paused = true
	self.deadVictims = 0

	self.images = {
		stage = love.graphics.newImage('assets/stage_pix.png')
	}

	self.sounds = {
		noise = love.audio.newSource('assets/bg-total.ogg')
	}
	self.sounds.noise:setLooping(true)
	self.sounds.noise:play()
end

function Game:score()
	local score = 0
	for i, v in ipairs(self.victims) do
		if v.dead == true then
			score = score + 150
		else
			score = score + (120 - v.health)
		end
	end

	return math.floor(score)
end

function Game:update(dt)
	self.currentTime = love.timer.getTime()

	if self.over == true or self.paused == true then
		return
	end

	self.singer:update(dt)
	self.notes:update(dt)

	for i, v in ipairs(self.victims) do
		v:update(dt)
	end

	self.timeLeft = self.timeLeft - dt

	local ln2 = math.log(2)
	local speedArgument = (60 - self.timeLeft) / 60 * ln2
	self.notes.speed = 100 + (math.exp(speedArgument) - 1) * 50

	if self.timeLeft < 0 then
		self.over = true
		self.timeLeft = 0

		love.audio.stop()
		self.singer:singEnd()
	end
end

function Game:draw()
	love.graphics.draw(self.images.stage, 0, 0)

	self.singer:draw()
	
	for i, v in ipairs(self.victims) do
		v:draw()
	end

	self.notes:draw()

	-- display timer
	local minutes = math.floor(self.timeLeft / 60)
	local sec = math.floor(self.timeLeft) % 60
	love.graphics.print(string.format('%02d:%02d', minutes, sec), 10, 10)
end

function Game:play()
	if self.paused == true then
		self.notes:start()
		self.timerStart = love.timer.getTime()
		self.paused = false

		self.singer:clearThroat()
	end
end

function Game:damageSpectators(power)
	for i, v in ipairs(self.victims) do
		local sx, sy = self.singer.pos.x, self.singer.pos.y
		local vx, vy = v.pos.x, v.pos.y
		local dist = ((vx-sx)^2 + (vy-sy)^2)^0.5
		v:randomlyBleed(power, dist)
	end
end

function Game:getSinger()
	return self.singer
end

function Game:deathProgress()
	return self.deadVictims / #self.victims
end

function Game:registerDeath()
	self.deadVictims = self.deadVictims + 1
end

return Game