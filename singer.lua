local class = require 'lib/middleclass'

local Singer = class('Singer')

function Singer:initialize(id)
	self.id = id or 0
	self.images = {
		open = love.graphics.newImage('assets/soprano-open.png'),
		closed = love.graphics.newImage('assets/soprano-closed.png')
	}

	local imW, imH = self.images.open:getDimensions()
	self.pos = {
		x = OD.width / 2 - imW / 2,
		y = 60
	}
	self.color = {200, 200, 128}

	self.sounds = {
		throat = love.audio.newSource('assets/throat.wav', 'static')
	}

	self.notes = {
		e8 = love.audio.newSource('assets/voice-e8.wav', 'static'),
		g8 = love.audio.newSource('assets/voice-g8.wav', 'static'),
		b8 = love.audio.newSource('assets/voice-b8.wav', 'static'),
		c9 = love.audio.newSource('assets/voice-c9.wav', 'static'),
		figaro = love.audio.newSource('assets/figaro.wav', 'static'),
		scale = love.audio.newSource('assets/voice-scale.wav', 'static'),
	}
	self.singing = false
	self.lastSung = love.timer.getTime()
	
	self.keys = {}
	for k,_ in pairs(self.notes) do
		table.insert(self.keys, k)
	end
end

function Singer:update(dt)
	local now = love.timer.getTime()

	if self.singing == true and self.lastSung + 3.0 < now then
		self.singing = false
	end
end

function Singer:draw()
	love.graphics.setColor(self.color)
	-- love.graphics.rectangle('fill', self.pos.x, self.pos.y, 20, 20)
	love.graphics.setColor(255, 255, 255, 255)

	if self.singing == true then
		local shakeAmp = 3 - love.timer.getTime() + self.lastSung
		local shakeX = math.sin(love.timer.getTime() * 50) * shakeAmp * 2
		love.graphics.draw(self.images.open, self.pos.x + shakeX, self.pos.y)
	else
		love.graphics.draw(self.images.closed, self.pos.x, self.pos.y)
	end
end

function Singer:clearThroat()
	self.sounds.throat:play()
end

function Singer:sing()
	local note = love.math.random(1, #self.keys)

	self.notes[self.keys[note]]:play()
	self.singing = true
	self.lastSung = love.timer.getTime()
end

function Singer:singEnd()
	self.notes['e8']:play()
	self.notes['g8']:play()
	self.notes['c9']:play()

	self.singing = true
	self.lastSung = love.timer.getTime()
end

return Singer