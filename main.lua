OD = {
	states = {}
}

local Game = require('game')
GamestateMgr = require 'lib/gamestate'
require('menustate')
require('gamestate')
require('gameoverstate')

function love.load()
	OD.fonts = {
		normal = love.graphics.newFont('font/font.ttf', 20),
		title = love.graphics.newFont('font/font.ttf', 30)
	}
	love.graphics.setFont(OD.fonts.normal)

	OD.width, OD.height = love.window.getDimensions()

	-- OD.game = Game:new()
	OD._debug = false

	local icon = love.image.newImageData('assets/icon.png')
	love.window.setIcon(icon)

	GamestateMgr.registerEvents()
	GamestateMgr.switch(OD.states.menu)
end

function love.update(dt)
end

function love.draw()
end

function love.keyreleased(key)
	if key == 'escape' then
		love.event.quit()
	end
end