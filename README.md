# 60 Second Opera Death #
## A Ludum Dare #32 entry -- [game page](http://ludumdare.com/compo/ludum-dare-32/?action=preview&uid=21955) ##

What's the most people you can kill with your voice? 

The Unconventional Weapon is your voice. You're an evil opera singer with a plan: you want to kill your audience! You have the gift of terrible singing, so why not using that to wreak havoc? 

Use the keys A, S, D, F and H, J, K, L to hit the scrolling notes at the right time! 

ESC quits the game. 

**Make sure to turn up your volume!**

Disclaimer: I was originally planning to use the JKL; keys like a normal person :p, but the font didn't display the semicolon in an obvious way and instead opted for shifting the keys one letter to the left! 

Linux/Mac users: Download LOVE from [the official website](https://love2d.org/) and run ```60SecondOD.love``` using the instructions in the 'Running Games' section [in the wiki](https://www.love2d.org/wiki/Getting_Started).

### Downloads & Links ###
* [Windows 32-bit](https://www.dropbox.com/s/70ap4er6ld2u7f3/dist%2032.zip?dl=0)
* [Windows 64-bit](https://www.dropbox.com/s/zjtw6imfdz5sr3w/dist%2064.zip?dl=0)
* [OS X / Linux](https://www.dropbox.com/s/ug36fr5vglcuda6/60SecondOD.love?dl=0)

### Ratings ###

![score_crop.png](https://bitbucket.org/repo/n5Lrko/images/3320216631-score_crop.png)