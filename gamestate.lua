local Game = require('game')

OD.states.game = {}

function OD.states.game:init()
	OD.game = Game:new()

	self.canvas = love.graphics.newCanvas()
	self.shader = love.graphics.newShader[[
		extern number progress;

		vec3 contrast(vec3 rgb, number x) {
			return ((rgb - 0.5) * x) + 0.5;
		}

	    vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
	      vec4 pixel = Texel(texture, texture_coords );//This is the current pixel color
	      vec4 col = pixel * color;
	      vec4 lumcoeff = vec4(0.299, 0.587, 0.114, 0);

	      vec4 deathCol = vec4(contrast(vec3(dot(col, lumcoeff)), 1.8), col.a);
	      return mix(col, deathCol, progress * 0.8);
	    }
  	]]

  	self.progress = 0

  	self.noise = love.audio.newSource('assets/noise.wav')
  	self.noise:setLooping(true)
  	self.noise:setVolume(0)
end

function OD.states.game:enter()
	OD.game:play()
	self.noise:play()
end

function OD.states.game:update(dt)
	OD.game:update(dt)

	if self.progress < OD.game:deathProgress() then
		self.progress = self.progress + dt * (OD.game:deathProgress() - self.progress) * 4
	end

	self.noise:setVolume(self.progress * 0.8)

	if OD.game.over then
		self.noise:play()
		self.noise:setVolume(1)
		GamestateMgr.switch(OD.states.gameover, OD.game.deadVictims, OD.game:score())
	end
end

function OD.states.game:draw()
	love.graphics.setShader(self.shader)
	self.shader:send('progress', self.progress)
	OD.game:draw()

	love.graphics.setShader()
end

function OD.states.game:keyreleased(key)
	if key == ' ' then
		OD.game:play()
	end
end