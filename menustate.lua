OD.states.menu = {}

function OD.states.menu:init()
end

function OD.states.menu:update(dt)

end

function OD.states.menu:draw()
	local string = '60 Second Opera Death'
	local stringW = OD.fonts.title:getWidth(string)

	love.graphics.setFont(OD.fonts.title)
	love.graphics.setColor(137, 7, 7)
	love.graphics.print(string, OD.width / 2 - stringW / 2, 30)
	love.graphics.setColor(255, 255, 255)

	string = 'Play [Enter]'
	stringW = OD.fonts.normal:getWidth(string)
	love.graphics.setFont(OD.fonts.normal)
	love.graphics.print(string, OD.width / 2 - stringW / 2, OD.height / 2 - 40)

	string = 'You\'re an evil opera singer, destined to kill everyone watching your performance... Use A, S, D, F and H, J, K, L to hit the right notes!'
	stringW = OD.fonts.normal:getWidth(string)
	love.graphics.setFont(OD.fonts.normal)
	love.graphics.printf(string, OD.width / 2 - 200, OD.height / 2 - 180, 400)
end

function OD.states.menu:keyreleased(key)
	if key == 'return' then
		GamestateMgr.switch(OD.states.game)
	end
end