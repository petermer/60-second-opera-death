-- timeline for notes
local class = require 'lib/middleclass'

local Notes = class('Notes')

-- valid note keys
Notes.static.validKeys = {'A', 'S', 'D', 'F', 'J', 'K', 'L', 'H'}
Notes.static.delays = {1, 2, 4, 6, 8, 10}

function Notes:initialize()
	self.startTime = love.timer.getTime()
	self.paused = true

	self.images = {
		love.graphics.newImage('assets/note-1.png'),
		love.graphics.newImage('assets/note-2.png')
	}

	self.notes = {}
	local prevDelay = 0
	local nNotes = 140
	for i=1,nNotes do
		self.notes[i] = {}
		self.notes[i].key = Notes.validKeys[love.math.random(1, #Notes.validKeys)]
		prevDelay = prevDelay + Notes.delays[love.math.random(1, #Notes.delays)]/8
		self.notes[i].delay = prevDelay
		self.notes[i].passed = false
		self.notes[i].hit = false
		self.notes[i].img = self.images[love.math.random(1, 2)]
		self.notes[i].height = love.math.random(1, 4) -- 4 different heights
	end

	self.offset = 1.5 -- sec
	self.speed = 120 -- pixel/sec
	self.currentPos = -self.offset
	self.missed = 0
end

function Notes:start()
	self.startTime = love.timer.getTime()
	self.currentPos = 0
	self.paused = false
end

function Notes:damage(power)
	OD.game:damageSpectators(power / 100)
end

function Notes:update(dt)
	if self.paused == true then
		return
	end

	-- local offset = 0.4 * OD.width/self.speed
	self.currentPos = love.timer.getTime() - self.startTime - self.offset

	local debounce = false
	for i, n in ipairs(self.notes) do
		local window = 0.1

		if debounce == true then
			return
		end

		local fontOffset = 5/self.speed
		if n.passed == false and love.keyboard.isDown(n.key:lower()) then
			if n.delay - window + fontOffset <= self.currentPos and n.delay + window + fontOffset >= self.currentPos then
				n.passed = true
				n.hit = true
				self:damage(100 - math.abs(n.delay - self.currentPos) * 100/window)
				debounce = true

				OD.game:getSinger():sing()
			end
		end

		if n.passed == false then
			if n.delay + window + fontOffset < self.currentPos then
				self.missed = self.missed + 1
				n.passed = true
			end
		end
	end
end

function Notes:draw()
	local yStart = 230
	for i, n in ipairs(self.notes) do
		love.graphics.setColor({255,255,255})
		-- love.graphics.print(math.floor(n.delay*8), i * 20, 15)

		if n.hit == true then
			love.graphics.setColor({20, 240, 20})
		else
			love.graphics.setColor({240, 20, 20})
		end

		if n.passed == false then
			love.graphics.setColor({255, 255, 255})
		end

		love.graphics.draw(n.img, OD.width / 2 - self.speed*(self.currentPos - n.delay) - 4, yStart + n.height * 4 + 10)
		love.graphics.print(n.key, OD.width / 2 - self.speed*(self.currentPos - n.delay), yStart + 45)
	end

	-- indicator line
	love.graphics.setColor({255,255,255, 200})
	love.graphics.rectangle('fill', OD.width / 2 - 2, yStart + 15, 4, 55)

	-- debug
	if OD._debug then
		love.graphics.print(self.currentPos, OD.width - 60, 10)
		love.graphics.print(self.missed, OD.width - 20, 30)
	end
end

return Notes