OD.states.gameover = {}

function OD.states.gameover:init()
	self.kills = 0
	self.grade = 'D'
end

function OD.states.gameover:enter(current, kills, score)
	self.kills = kills
	self.score = score
	
	if self.kills < 6 then
		self.grade = 'D'
	elseif self.kills < 15 then
		self.grade = 'C'
	elseif self.kills < 35 then
		self.grade = 'B'
	else
		self.grade = 'A'
	end
end

function OD.states.gameover:draw()
	love.graphics.setColor(137, 7, 7)
	love.graphics.print('You killed: ' .. self.kills .. ' spectators', 300, 200)

	love.graphics.setFont(OD.fonts.title)
	-- love.graphics.print('GRADE: ' .. self.grade, 300, 240)
	love.graphics.print('Score: ' .. self.score, 300, 240)
end